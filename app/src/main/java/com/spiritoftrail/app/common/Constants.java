package com.spiritoftrail.app.common;

public class Constants {

    private Constants() {
        throw new IllegalStateException("Constants class");
    }

    public static final String URL_BASE_COMMENT = "https://spiritoftrail.fr/rss/comments.php?id=";

    public static final String INTENT_GENDER = "GENDER";
}
