package com.spiritoftrail.app.activity;

import android.app.Activity;

import android.content.Intent;

import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.view.inputmethod.InputMethodManager;

import android.widget.Button;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import com.spiritoftrail.app.utils.AnalyticsApplication;
import com.spiritoftrail.app.asynctask.HttpRequest;
import com.spiritoftrail.app.R;

public class ActivityRegister extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        /* Google Analytics */
        String screen = "HOME";
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(screen);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder().setCategory("Action").setAction("Share").build());

        Button send = findViewById(R.id.register_send);

        TextInputLayout tilFirstname = findViewById(R.id.register_til_firstname);
        TextInputLayout tilLastname = findViewById(R.id.register_til_lastname);
        TextInputLayout tilPassword = findViewById(R.id.register_til_password);
        TextInputLayout tilRepassword = findViewById(R.id.register_til_repassword);
        TextInputLayout tilEmail = findViewById(R.id.register_til_email);
        TextInputLayout tilBirthday = findViewById(R.id.register_til_birthday);

        HttpRequest request = new HttpRequest();

        send.setOnClickListener(view -> {
            /* Enlève le clavier */
            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            String firstname = null;
            String lastname = null;
            String password = null;
            String repassword = null;
            String email = null;
            String birthday = null;

            if((tilFirstname.getEditText()) != null)
                firstname = tilFirstname.getEditText().getText().toString();

            if((tilLastname.getEditText()) != null)
                 lastname = tilLastname.getEditText().getText().toString();

            if((tilPassword.getEditText()) != null)
                password = tilPassword.getEditText().getText().toString();

            if((tilRepassword.getEditText()) != null)
                repassword = tilRepassword.getEditText().getText().toString();

            if((tilEmail.getEditText()) != null)
                email = tilEmail.getEditText().getText().toString();

            if((tilBirthday.getEditText()) != null)
                birthday = tilBirthday.getEditText().getText().toString();

            String hostname = "https://spiritoftrail.fr/android/register.php";
            String params = "prenom=" + firstname + "&nom=" + lastname + "&password=" + password +
                    "&re_password=" + repassword + "&email=" + email + "&annee=" + birthday + "&pays=France&send=true";

            String result = request.post(hostname, params);

            /* Suppression des précédents messages d'erreur */
            tilPassword.setErrorEnabled(false);
            tilEmail.setErrorEnabled(false);

            switch(result) {
                /* Inscription réussie */
                case "0":
                    Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                    intent.putExtra("REGISTER", email);
                    startActivity(intent);
                    finish();
                    break;

                /* Les mots de passes saisis sont différents */
                case "1":
                    tilPassword.setError("Les mots de passes saisis sont différents");
                    break;

                /* L'email est déjà utilisé */
                case "2":
                    tilEmail.setError("Cette adresse email est déjà utilisé par un autre compte");
                    break;

                /* Erreur interne au serveur */
                default:
                    break;
            }
        });
    }
}
