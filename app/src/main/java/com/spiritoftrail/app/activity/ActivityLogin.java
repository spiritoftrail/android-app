package com.spiritoftrail.app.activity;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import android.view.View;

import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.RelativeLayout;

import com.spiritoftrail.app.R;
import com.spiritoftrail.app.asynctask.LoginAsyncTask;
import com.spiritoftrail.app.session.SessionManager;
import com.spiritoftrail.app.utils.Keyboard;

public class ActivityLogin extends AppCompatActivity {

    private final Context context = this;
    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;
    private RelativeLayout relativeLogin;
    private RelativeLayout relativeInfo;
    private EditText loginEmail;
    private EditText loginPassword;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Intent intent = getIntent();
        if(intent.hasExtra("REGISTER")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Un email a été envoyé à l'adresse " + intent.getStringExtra("REGISTER") + " afn d'activer votre compte");
            builder.create().show();
        }

        Button send = findViewById(R.id.login_send);
        Button register = findViewById(R.id.register);

        relativeLogin = findViewById(R.id.relative_login);
        relativeInfo = findViewById(R.id.relative_info);

        tilEmail = findViewById(R.id.login_til_email);
        tilPassword = findViewById(R.id.login_til_password);

        loginEmail = findViewById(R.id.login_email);
        loginPassword = findViewById(R.id.login_password);

        /* Connexion automatique - Session existante */
        session = new SessionManager(this);
        if(session.isLogged())
            login(session.getEmail(), session.getPassword());

        send.setOnClickListener(view -> {
            String email = null;
            String password = null;

            if((tilEmail.getEditText()) != null)
                email = tilEmail.getEditText().getText().toString();

            if((tilPassword.getEditText()) != null)
                password = tilPassword.getEditText().getText().toString();

            login(email, password);
        });

        register.setOnClickListener(view -> {
            Intent intent1 = new Intent(getApplicationContext(), ActivityRegister.class);
            startActivity(intent1);
        });
    }

    public void login(String email, String password) {
        /* Suppression du clavier */
        Keyboard.hideKeyboard(ActivityLogin.this);

        /* Suppression du curseur */
        loginEmail.setCursorVisible(false);
        loginPassword.setCursorVisible(false);

        /* Suppression des précédents messages d'erreur */
        tilEmail.setErrorEnabled(false);
        tilPassword.setErrorEnabled(false);

        relativeLogin.setVisibility(View.GONE);
        relativeInfo.setVisibility(View.VISIBLE);

        LoginAsyncTask asyncTask = new LoginAsyncTask(context, relativeLogin, relativeInfo, tilEmail, tilPassword, loginEmail, loginPassword, session, email, password);

        String hostname = "https://spiritoftrail.fr/android/login.php";
        String params = "email=" + email + "&password=" + password + "&send=true";

        asyncTask.execute(hostname, params);
    }
}
