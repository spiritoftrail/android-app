package com.spiritoftrail.app.activity;

import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;

import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import com.spiritoftrail.app.utils.AnalyticsApplication;
import com.spiritoftrail.app.adapters.BuilderAdapter;
import com.spiritoftrail.app.R;
import com.spiritoftrail.app.adapters.XMLAsyncTask;
import com.spiritoftrail.app.utils.Network;

public class ActivityBuilder extends AppCompatActivity {

    XMLAsyncTask asyncTask = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_builder);

        /* Google Analytics */
        String screen = "HOME";
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(screen);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder().setCategory("Action").setAction("Share").build());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();
        String id = null;

        if(bundle != null)
            id = bundle.getString("id");

       /* Connexion réseau disponible */
        if(Network.checkNetwork(this)) {
            final RecyclerView wrapper = findViewById(R.id.view);
            wrapper.setLayoutManager(new LinearLayoutManager(this));
            BuilderAdapter adapter = new BuilderAdapter();
            wrapper.setAdapter(adapter);

            asyncTask = new XMLAsyncTask(this, adapter);
            asyncTask.execute("https://spiritoftrail.fr/rss/builder.php?id=" + id);

            /* Chargement de la page */
            final ProgressBar progress = findViewById(R.id.progress);
            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    progress.setVisibility(View.GONE);
                }
            });
        }
        else
            Toast.makeText(this, "Aucune connexion réseau", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
}
