package com.spiritoftrail.app.activity;

import android.app.Activity;

import android.os.Bundle;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import com.spiritoftrail.app.common.Constants;
import com.spiritoftrail.app.utils.AnalyticsApplication;
import com.spiritoftrail.app.adapters.CommentAdapter;
import com.spiritoftrail.app.asynctask.HttpRequest;
import com.spiritoftrail.app.R;
import com.spiritoftrail.app.session.SessionManager;
import com.spiritoftrail.app.adapters.XMLAsyncTask;
import com.spiritoftrail.app.utils.Network;

public class ActivityComment extends AppCompatActivity {

    private HttpRequest request;
    XMLAsyncTask asyncTask = null;
    CommentAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_comment);

        /* Google Analytics */
        String screen = "HOME";
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(screen);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder().setCategory("Action").setAction("Share").build());

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        ActionBar actionBar = getSupportActionBar();

        /* Chargement de la session */
        final SessionManager session = new SessionManager(this);

        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();

        if(bundle != null) {
            final String id = getIntent().getExtras().getString("id");
            final String page = getIntent().getExtras().getString("page");

            /* Connexion réseau disponible */
            if(Network.checkNetwork(this)) {
                final RecyclerView wrapper = findViewById(R.id.view);
                wrapper.setLayoutManager(new LinearLayoutManager(this));
                adapter = new CommentAdapter();
                wrapper.setAdapter(adapter);

                /* Chargement des commentaires */
                loadComments(page, id);

                /* Chargement de la page */
                final ProgressBar progress = findViewById(R.id.progress);
                adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onChanged() {
                        progress.setVisibility(View.GONE);
                    }
                });
            } else
                Toast.makeText(this, "Aucune connexion réseau", Toast.LENGTH_LONG).show();

            EditText comment = findViewById(R.id.comment_edit);
            comment.setOnEditorActionListener((view, actionId, event) -> {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    String text = comment.getText().toString();
                    comment.getText().clear();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(comment.getWindowToken(), 0);
                    comment.clearFocus();

                    /* Publication du commentaire */
                    request = new HttpRequest();

                    String hostname = "https://spiritoftrail.fr/android/comment.php?id=" + id;
                    String params = "uid=" + session.getId() + "&textarea=" + text + "&page=" + page + "&send=true";
                    request.post(hostname, params);

                    loadComments(page, id);
                }

                return false;
            });
        }
    }

    public void loadComments(String page, String id) {
        final String url = switch (page) {
            case "news" -> Constants.URL_BASE_COMMENT + id;
            case "story" -> Constants.URL_BASE_COMMENT + id + "&id_type=2";
            default -> Constants.URL_BASE_COMMENT + id;
        };

        asyncTask = new XMLAsyncTask(this, adapter);
        asyncTask.execute(url);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(asyncTask !=  null)
            asyncTask.cancel(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
}
