package com.spiritoftrail.app.activity;

import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.widget.ProgressBar;
import android.widget.Toast;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import com.spiritoftrail.app.utils.AnalyticsApplication;
import com.spiritoftrail.app.adapters.BuildingAdapter;
import com.spiritoftrail.app.R;
import com.spiritoftrail.app.session.SessionManager;
import com.spiritoftrail.app.adapters.XMLAsyncTask;
import com.spiritoftrail.app.utils.Network;
import com.spiritoftrail.app.menu.SMenu;

public class ActivityBuilding extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private SessionManager session;
    XMLAsyncTask asyncTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building);

        /* Google Analytics */
        String screen = "HOME";
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(screen);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder().setCategory("Action").setAction("Share").build());

        NavigationView navigationView = findViewById(R.id.nav_view);
        SMenu menu = new SMenu(this, navigationView);

        /* Chargement de la session */
        session = new SessionManager(this);
        if(session.isLogged())
            menu.setAppBarLayout(session);

        menu.setMenu();

        /* Barre de menu */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        /* Connexion réseau disponible */
        if(Network.checkNetwork(this)) {
            final RecyclerView wrapper = findViewById(R.id.view);
            wrapper.setLayoutManager(new LinearLayoutManager(this));
            BuildingAdapter adapter = new BuildingAdapter();
            wrapper.setAdapter(adapter);

            asyncTask = new XMLAsyncTask(this, adapter);
            asyncTask.execute("https://spiritoftrail.fr/rss/building.php");

            /* Chargement de la page */
            final ProgressBar progress = findViewById(R.id.progress);
            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    progress.setVisibility(View.GONE);
                }
            });
        }
        else
            Toast.makeText(this, "Aucune connexion réseau", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(asyncTask !=  null)
            asyncTask.cancel(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if(drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_settings) {
            session.logout();
            Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;

        if(item.getItemId() == R.id.nav_news) {
            intent = new Intent(getApplicationContext(), ActivityNews.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.nav_trainers) {
            intent = new Intent(getApplicationContext(), ActivityTrainers.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.nav_stories) {
            intent = new Intent(getApplicationContext(), ActivityStories.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.nav_sheets) {
            intent = new Intent(getApplicationContext(), ActivitySheets.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.nav_directory) {
            intent = new Intent(getApplicationContext(), ActivityDirectory.class);
            startActivity(intent);
        }

        new Handler().postDelayed(() -> {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }, 100);

        return true;
    }
}
