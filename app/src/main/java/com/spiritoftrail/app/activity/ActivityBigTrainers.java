package com.spiritoftrail.app.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import com.spiritoftrail.app.utils.AnalyticsApplication;
import com.spiritoftrail.app.R;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class ActivityBigTrainers extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bigtrainers);

        /* Google Analytics */
        String screen = "HOME";
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(screen);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder().setCategory("Action").setAction("Share").build());

        Bundle bundle = getIntent().getExtras();

        if(bundle != null) {
            String path = bundle.getString("path");

            ImageView image;
            image = findViewById(R.id.image);

            Picasso.with(getApplicationContext()).load("https://spiritoftrail.fr/upload/image/" + path).into(image);
        }
    }
}
