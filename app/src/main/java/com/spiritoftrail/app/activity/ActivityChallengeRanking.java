package com.spiritoftrail.app.activity;

import android.os.Bundle;

import android.view.MenuItem;

import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import com.spiritoftrail.app.R;
import com.spiritoftrail.app.adapters.ChallengeRanking;
import com.spiritoftrail.app.adapters.ChallengeRankingAdapter;
import com.spiritoftrail.app.asynctask.ChallengeRankingAsyncTask;
import com.spiritoftrail.app.common.Constants;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

public class ActivityChallengeRanking extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progress;
    private final List<ChallengeRanking> ranking = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_challenge_ranking);

        Bundle extras = getIntent().getExtras();

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        ChallengeRankingAdapter adapter = new ChallengeRankingAdapter(ranking);

        recyclerView = findViewById(R.id.recyclerView);
        progress = findViewById(R.id.progress);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        TextView title = findViewById(R.id.rankTitle);

        if(extras != null) {
            switch (extras.getInt(Constants.INTENT_GENDER)) {
                case 0 -> title.setText(R.string.challenge_ranking_g);
                case 1 -> title.setText(R.string.challenge_ranking_m);
                case 2 -> title.setText(R.string.challenge_ranking_w);
                default -> title.setText(R.string.challenge_ranking_g);
            }

            getGRanking(extras.getInt(Constants.INTENT_GENDER));
        }
    }

    public void getGRanking(int gender) {
        ChallengeRankingAsyncTask asyncTask = new ChallengeRankingAsyncTask(this, recyclerView, ranking, progress);
        String params = "gender=" + gender + "&send=true";
        String hostname = "https://www.spiritoftrail.fr/android/challenge.php";

        asyncTask.execute(hostname, params);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
}
