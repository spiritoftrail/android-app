package com.spiritoftrail.app.activity;

import android.os.Bundle;

import android.view.MenuItem;

import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import com.spiritoftrail.app.R;
import com.spiritoftrail.app.adapters.ChallengeCourses;
import com.spiritoftrail.app.adapters.ChallengeCoursesAdapter;
import com.spiritoftrail.app.asynctask.ChallengeCoursesAsyncTask;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

public class ActivityChallengeCourses extends AppCompatActivity {

    private final List<ChallengeCourses> courses = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_challenge_courses);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        ChallengeCoursesAdapter adapter = new ChallengeCoursesAdapter(courses);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        ProgressBar progress = findViewById(R.id.progress);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        ChallengeCoursesAsyncTask asyncTask = new ChallengeCoursesAsyncTask(this, recyclerView, courses, progress);
        String params = "courses=true&send=true";
        String hostname = "https://www.spiritoftrail.fr/android/challenge.php";

        asyncTask.execute(hostname, params);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
}
