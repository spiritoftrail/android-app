package com.spiritoftrail.app.activity;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;

import android.widget.Button;
import android.widget.ProgressBar;

import com.spiritoftrail.app.asynctask.ChallengeAsyncTask;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.view.Menu;
import android.view.MenuItem;

import com.spiritoftrail.app.R;
import com.spiritoftrail.app.common.Constants;
import com.spiritoftrail.app.session.SessionManager;
import com.spiritoftrail.app.utils.AnalyticsApplication;
import com.spiritoftrail.app.menu.SMenu;

import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

public class ActivityChallenge extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private final Context context = this;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge);

        /* Google Analytics */
        String screen = "HOME";
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(screen);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder().setCategory("Action").setAction("Share").build());

        NavigationView navigationView = findViewById(R.id.nav_view);
        SMenu menu = new SMenu(this, navigationView);

        /* Chargement de la session */
        session = new SessionManager(this);
        if(session.isLogged())
            menu.setAppBarLayout(session);

        menu.setMenu();

        /* Barre de menu */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        TextView title = findViewById(R.id.progressTitle);

        ProgressBar progressBar = findViewById(R.id.progressBar);
        ProgressBar progress = findViewById(R.id.progress);

        Button races = findViewById(R.id.races);
        Button granking = findViewById(R.id.granking);
        Button mranking = findViewById(R.id.mranking);
        Button franking = findViewById(R.id.franking);

        races.setOnClickListener(v -> {
            Intent intent = new Intent(context, ActivityChallengeCourses.class);
            startActivity(intent);
        });

        granking.setOnClickListener(v -> {
            Intent intent = new Intent(context, ActivityChallengeRanking.class);
            intent.putExtra(Constants.INTENT_GENDER, 0);
            startActivity(intent);
        });

        mranking.setOnClickListener(v -> {
            Intent intent = new Intent(context, ActivityChallengeRanking.class);
            intent.putExtra(Constants.INTENT_GENDER, 1);
            startActivity(intent);
        });

        franking.setOnClickListener(v -> {
            Intent intent = new Intent(context, ActivityChallengeRanking.class);
            intent.putExtra(Constants.INTENT_GENDER, 2);
            startActivity(intent);
        });

        ChallengeAsyncTask asyncTask = new ChallengeAsyncTask(this, title, progressBar, progress);
        String params = "progress=true&send=true";
        String hostname = "https://www.spiritoftrail.fr/android/challenge.php";

        asyncTask.execute(hostname, params);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if(drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_settings) {
            session.logout();
            Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;

        if(item.getItemId() == R.id.nav_news) {
            intent = new Intent(getApplicationContext(), ActivityNews.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.nav_trainers) {
            intent = new Intent(getApplicationContext(), ActivityTrainers.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.nav_stories) {
            intent = new Intent(getApplicationContext(), ActivityStories.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.nav_building) {
            intent = new Intent(getApplicationContext(), ActivityBuilding.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.nav_sheets) {
            intent = new Intent(getApplicationContext(), ActivitySheets.class);
            startActivity(intent);
        } else if(item.getItemId() == R.id.nav_directory) {
            intent = new Intent(getApplicationContext(), ActivityDirectory.class);
            startActivity(intent);
        }

        new Handler().postDelayed(() -> {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }, 100);

        return true;
    }
}
