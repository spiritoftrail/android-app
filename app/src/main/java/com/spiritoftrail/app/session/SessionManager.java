package com.spiritoftrail.app.session;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class SessionManager {
    private SharedPreferences prefs = null;
    private SharedPreferences.Editor editor = null;
    private static final String PREFS_NAME = "app_prefs";
    private static final String IS_LOGGED = "isLogged";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String GROUP = "group";
    private static final String ID = "id";

    public SessionManager(Context context) {
        try {
            String masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);

            prefs = EncryptedSharedPreferences.create(
                    PREFS_NAME,
                    masterKeyAlias,
                    context,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            );

            editor = prefs.edit();
        } catch (GeneralSecurityException | IOException ignored) {}
    }

    public boolean isLogged() {
        return prefs.getBoolean(IS_LOGGED, false);
    }

    public String getFirstname() {
        return prefs.getString(FIRSTNAME, null);
    }

    public String getLastname() {
        return prefs.getString(LASTNAME, null);
    }

    public String getEmail() {
        return prefs.getString(EMAIL, null);
    }

    public String getPassword() {
        return prefs.getString(PASSWORD, null);
    }

    public int getId() {
        return prefs.getInt(ID, 0);
    }

    public void insertUser(int id, String firstname, String lastname, String email, String password, String group) {
        editor.putBoolean(IS_LOGGED, true);
        editor.putInt(ID, id);
        editor.putString(FIRSTNAME, firstname);
        editor.putString(LASTNAME, lastname);
        editor.putString(EMAIL, email);
        editor.putString(PASSWORD, password);
        editor.putString(GROUP, group);
        editor.commit();
    }

    public void logout() {
        editor.remove(IS_LOGGED).commit();
        editor.remove(ID).commit();
        editor.remove(FIRSTNAME).commit();
        editor.remove(LASTNAME).commit();
        editor.remove(EMAIL).commit();
        editor.remove(PASSWORD).commit();
        editor.remove(GROUP).commit();
    }
}
