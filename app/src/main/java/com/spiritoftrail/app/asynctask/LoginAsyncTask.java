package com.spiritoftrail.app.asynctask;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.os.AsyncTask;

import com.google.android.material.textfield.TextInputLayout;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.spiritoftrail.app.activity.ActivityNews;
import com.spiritoftrail.app.session.SessionManager;

import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginAsyncTask extends AsyncTask<String, Void, JSONObject> {

    private static final Logger logger = Logger.getLogger(LoginAsyncTask.class.getName());

    private final WeakReference<Context> context;
    private final WeakReference<RelativeLayout> relativeLogin;
    private final WeakReference<RelativeLayout> relativeInfo;
    private final WeakReference<TextInputLayout> tilEmail;
    private final WeakReference<TextInputLayout> tilPassword;
    private final WeakReference<TextView> loginEmail;
    private final WeakReference<TextView> loginPassword;
    private final SessionManager session;
    private final String email;
    private final String paswword;

    public LoginAsyncTask(Context context, RelativeLayout relative_login, RelativeLayout relative_info, TextInputLayout til_email, TextInputLayout til_password, TextView login_email, TextView login_password, SessionManager session, String email, String password) {
        this.context = new WeakReference<>(context);
        this.relativeLogin = new WeakReference<>(relative_login);
        this.relativeInfo = new WeakReference<>(relative_info);
        this.tilEmail = new WeakReference<>(til_email);
        this.tilPassword = new WeakReference<>(til_password);
        this.loginEmail = new WeakReference<>(login_email);
        this.loginPassword = new WeakReference<>(login_password);
        this.session = session;
        this.email = email;
        this.paswword = password;
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpsURLConnection cnx = (HttpsURLConnection) url.openConnection();

            cnx.setRequestMethod("POST");
            cnx.setDoOutput(true);

            DataOutputStream stream = new DataOutputStream(cnx.getOutputStream());
            stream.writeBytes(params[1]);
            stream.flush();
            stream.close();

            String line;

            BufferedReader data = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            StringBuilder output = new StringBuilder();

            while((line = data.readLine()) != null)
                output.append(line);

            return new JSONObject(output.toString());

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            Toast.makeText(context.get(), "Aucune connexion réseau", Toast.LENGTH_LONG).show();
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        if(result != null) {
            try {
                if((result.getBoolean("success"))) {
                    int id = result.getInt("id");

                    String firstname = result.getString("firstname");
                    String lastname = result.getString("lastname");
                    String group = result.getString("group");

                    session.insertUser(id, firstname, lastname, email, paswword, group);

                    Intent intent = new Intent(context.get(), ActivityNews.class);
                    context.get().startActivity(intent);
                    ((Activity) context.get()).finish();
                } else {
                    loginEmail.get().setCursorVisible(true);
                    loginPassword.get().setCursorVisible(true);

                    relativeLogin.get().setVisibility(View.VISIBLE);
                    relativeInfo.get().setVisibility(View.GONE);

                    switch (result.getInt("error")) {
                        case 0 -> tilPassword.get().setError("Erreur de connexion. Veuilez réessayer");
                        case 1 -> tilEmail.get().setError("Mauvais identifiants");
                        case 2 -> tilPassword.get().setError("Vous devez activer votre compte pour continuer");
                        default -> tilEmail.get().setError("Erreur de connexion. Veuilez réessayer");
                    }
                }
            } catch(JSONException e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
