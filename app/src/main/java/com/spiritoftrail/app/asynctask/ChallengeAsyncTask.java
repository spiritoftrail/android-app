package com.spiritoftrail.app.asynctask;

import java.lang.ref.WeakReference;

import android.content.Context;

import android.content.res.Resources;
import android.os.AsyncTask;

import android.view.View;

import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.spiritoftrail.app.R;

import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChallengeAsyncTask extends AsyncTask<String, Void, JSONObject> {

    private static final Logger logger = Logger.getLogger(ChallengeAsyncTask.class.getName());

    private final WeakReference<Context> context;
    private final WeakReference<TextView> title;
    private final WeakReference<ProgressBar> progressBar;
    private final WeakReference<ProgressBar> progress;

    public ChallengeAsyncTask(Context context, TextView title, ProgressBar progressBar, ProgressBar progress) {
        this.context = new WeakReference<>(context);
        this.title = new WeakReference<>(title);
        this.progressBar = new WeakReference<>(progressBar);
        this.progress = new WeakReference<>(progress);
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpsURLConnection cnx = (HttpsURLConnection) url.openConnection();

            cnx.setRequestMethod("POST");
            cnx.setDoOutput(true);

            DataOutputStream stream = new DataOutputStream(cnx.getOutputStream());
            stream.writeBytes(params[1]);
            stream.flush();
            stream.close();

            String line;

            BufferedReader data = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            StringBuilder output = new StringBuilder();

            while((line = data.readLine()) != null)
                output.append(line);

            return new JSONObject(output.toString());

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            Toast.makeText(context.get(), "Aucune connexion réseau", Toast.LENGTH_LONG).show();
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        if(result != null) {
            try {
                if((result.getBoolean("success"))) {
                    Resources res = context.get().getResources();

                    int mcounter = result.getInt("mcounter");
                    int counter = result.getInt("counter");

                    title.get().setText(String.format(res.getString(R.string.challenge_progress), counter, mcounter));
                    progressBar.get().setMax(mcounter);
                    progressBar.get().setProgress(counter);
                }

            } catch(JSONException e) {
                logger.log(Level.SEVERE, e.getMessage());
            }

            progress.get().setVisibility(View.GONE);
        }
    }

}
