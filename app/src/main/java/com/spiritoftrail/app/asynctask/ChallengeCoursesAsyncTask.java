package com.spiritoftrail.app.asynctask;

import java.lang.ref.WeakReference;

import java.util.List;

import android.content.Context;

import android.os.AsyncTask;

import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import android.widget.ProgressBar;
import android.widget.Toast;

import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

import com.spiritoftrail.app.adapters.ChallengeCourses;
import com.spiritoftrail.app.adapters.ChallengeCoursesAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChallengeCoursesAsyncTask extends AsyncTask<String, Void, JSONObject> {

    private static final Logger logger = Logger.getLogger(ChallengeCoursesAsyncTask.class.getName());

    private final WeakReference<Context> context;
    private final WeakReference<RecyclerView> recyclerView;
    private final WeakReference<ProgressBar> progress;
    private final List<ChallengeCourses> courses;

    public ChallengeCoursesAsyncTask(Context context, RecyclerView recyclerView, List<ChallengeCourses> courses, ProgressBar progress) {
        this.context = new WeakReference<>(context);
        this.recyclerView = new WeakReference<>(recyclerView);
        this.progress = new WeakReference<>(progress);
        this.courses = courses;
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpsURLConnection cnx = (HttpsURLConnection) url.openConnection();

            cnx.setRequestMethod("POST");
            cnx.setDoOutput(true);

            DataOutputStream stream = new DataOutputStream(cnx.getOutputStream());
            stream.writeBytes(params[1]);
            stream.flush();
            stream.close();

            String line;

            BufferedReader data = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            StringBuilder output = new StringBuilder();

            while((line = data.readLine()) != null)
                output.append(line);

            return new JSONObject(output.toString());

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            Toast.makeText(context.get(), "Aucune connexion réseau", Toast.LENGTH_LONG).show();
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        if(result != null) {
            try {
                if((result.getBoolean("success"))) {
                    JSONArray jsonArray = result.getJSONArray("courses");

                    for(int i = 0; i < jsonArray.length(); i++) {
                        JSONObject index = jsonArray.getJSONObject(i);

                        int day = index.getInt("day");
                        int month = index.getInt("month");
                        String name = index.getString("name");

                        courses.add(new ChallengeCourses(day, month, name));
                    }

                    ChallengeCoursesAdapter adapter = new ChallengeCoursesAdapter(courses);
                    recyclerView.get().setAdapter(adapter);
                }
            } catch(JSONException e) {
                logger.log(Level.SEVERE, e.getMessage());
            }

            progress.get().setVisibility(View.GONE);
        }
    }
}
