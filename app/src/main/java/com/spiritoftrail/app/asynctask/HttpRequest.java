package com.spiritoftrail.app.asynctask;

import android.os.StrictMode;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

public class HttpRequest {

    private static final Logger logger = Logger.getLogger(HttpRequest.class.getName());

    public HttpRequest() {
        /* Do nothing */
    }

    public String post(String hostname, String params) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            URL url = new URL(hostname);
            HttpsURLConnection cnx = (HttpsURLConnection) url.openConnection();

            cnx.setRequestMethod("POST");
            cnx.setDoOutput(true);

            DataOutputStream stream = new DataOutputStream(cnx.getOutputStream());
            stream.writeBytes(params);
            stream.flush();
            stream.close();

            String line;

            BufferedReader data = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            StringBuilder output = new StringBuilder();

            while((line = data.readLine()) != null)
                output.append(line);

            return output.toString();

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }

        return "NULL";
    }
}
