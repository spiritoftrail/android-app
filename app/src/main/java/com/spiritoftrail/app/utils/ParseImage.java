package com.spiritoftrail.app.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.TextView;

import com.spiritoftrail.app.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParseImage implements Html.ImageGetter {

    private static final Logger logger = Logger.getLogger(ParseImage.class.getName());

    private final Context context;
    private final TextView text;

    public ParseImage(Context context, TextView text) {
        this.context = context;
        this.text = text;
    }

    @Override
    public Drawable getDrawable(String source) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = new ColorDrawable(context.getResources().getColor(R.color.transparent));
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

        new LoadImage().execute(source, d);

        return d;
    }

    class LoadImage extends AsyncTask<Object, Void, Bitmap> {
        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];

            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            /* Dimensions de l'écran */
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(metrics);
            int wscreen = metrics.widthPixels;

            /* Dimensions de l'image */
            int wimage = bitmap.getWidth();
            int himage = bitmap.getHeight();

            /* Calcul du ration en largeur de l'image par rapport à l'écran */
            float ratio = (float) wscreen / (float) wimage;

            /* Recalcul de la taille de l'image à afficher */
            int width = Math.round(ratio * wimage);
            int height = Math.round(ratio * himage);

            BitmapDrawable d = new BitmapDrawable(bitmap);
            mDrawable.addLevel(1, 1, d);
            mDrawable.setBounds(0, 0, width, height);
            mDrawable.setLevel(1);

            CharSequence t = text.getText();
            text.setText(t);
        }
    }
}
