package com.spiritoftrail.app.utils;

import java.util.HashMap;
import java.util.Map;

public class BBCode {

    private BBCode() {
        throw new UnsupportedOperationException();
    }

    public static String decode(String text) {
        String html = text;

        Map<String,String> bbMap = new HashMap<>();

        bbMap.put("(\r\n|\r|\n|\n\r)", "<br/>");
        bbMap.put("\\[b\\](.+?)\\[/b\\]", "<strong>$1</strong>");
        bbMap.put("\\[i\\](.+?)\\[/i\\]", "<span style=\"font-style:italic;\">$1</span>");
        bbMap.put("\\[u\\](.+?)\\[/u\\]", "<span style=\"text-decoration:underline;\">$1</span>");
        /* Missing strike text */
        bbMap.put("\\[left\\](.+?)\\[/left\\]", "<div align=\"left\">$1");
        bbMap.put("\\[right\\](.+?)\\[/right\\]", "<div align=\"right\">$1");
        bbMap.put("\\[center\\](.+?)\\[/center\\]", "<div align=\"center\">$1");
        bbMap.put("\\[justify\\](.+?)\\[/justify\\]", "<div align=\"justify\">$1");
        bbMap.put("\\[align=(.+?)\\](.+?)\\[/align\\]", "<div align=\"$1\">$2");
        bbMap.put("\\[color=(.+?)\\](.+?)\\[/color\\]", "<span style=\"color:$1;\">$2</span>");
        bbMap.put("\\[size=(.+?)\\](.+?)\\[/size\\]", "<span style=\"font-size:$1;\">$2</span>");
        bbMap.put("\\[img\\](.+?)\\[/img\\]", "<div style=\"text-align:center;\"><img src=\"$1\" alt=\"Image\" /></div>");
        bbMap.put("\\[url\\](.+?)\\[/url\\]", "<a href=\"$1\">$1</a>");
        bbMap.put("\\[url=(.+?)\\](.+?)\\[/url\\]", "<a href=\"$1\">$2</a>");
        bbMap.put("\\[ytb\\](.+?)\\[/ytb\\]", "<a href=\"https://www.youtube.com/embed/$1\">Vidéo Youtube</a>");
        bbMap.put("\\[vdo\\](.+?)\\[/vdo\\]", "<video controls src=\"$1\" width=\"640\" />");

        /* Absolute path */
        bbMap.put("(../upload/image/)", "https://spiritoftrail.fr/upload/image/");

        for (Map.Entry entry: bbMap.entrySet()) {
            html = html.replaceAll(entry.getKey().toString(), entry.getValue().toString());
        }

        return html;
    }
}
