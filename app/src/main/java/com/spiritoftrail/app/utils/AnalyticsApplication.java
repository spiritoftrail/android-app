package com.spiritoftrail.app.utils;

import android.app.Application;

import com.spiritoftrail.app.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class AnalyticsApplication extends Application {

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        sAnalytics = GoogleAnalytics.getInstance(this);
    }

    public synchronized Tracker getDefaultTracker() {
        if (sTracker == null)
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);

        return sTracker;
    }
}
