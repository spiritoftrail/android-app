package com.spiritoftrail.app.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.w3c.dom.Document;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilderFactory;

public class XMLAsyncTask extends AsyncTask<String, Void, Document> {
    interface DocumentConsumer {
        void setXMLDocument(Document document);
    }

    private static final Logger logger = Logger.getLogger(XMLAsyncTask.class.getName());

    private final DocumentConsumer mConsumer;
    private final WeakReference<Context> mContext;

    public XMLAsyncTask(Context context, DocumentConsumer consumer) {
        this.mContext = new WeakReference<>(context);
        this.mConsumer = consumer;
    }

    @Override
    protected Document doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);

            HttpsURLConnection cnx = (HttpsURLConnection) url.openConnection();

            InputStream stream = cnx.getInputStream();

            return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(stream);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            Toast.makeText(mContext.get(), "Aucune connexion réseau", Toast.LENGTH_LONG).show();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Document result) {
        if(result != null)
            mConsumer.setXMLDocument(result);
    }
}
