package com.spiritoftrail.app.adapters;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.spiritoftrail.app.R;
import com.spiritoftrail.app.activity.ActivityBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class BuildingAdapter extends RecyclerView.Adapter<BuildingAdapter.ArticleViewHolder> implements XMLAsyncTask.DocumentConsumer {

    private Document mDocument = null;

    @Override
    public int getItemCount() {
        if(mDocument != null)
            return mDocument.getElementsByTagName("item").getLength();
        else
            return 0;
    }

    @Override
    @NonNull
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_building, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        Element item = (Element) mDocument.getElementsByTagName("item").item(position);
        holder.setElement(item);
    }

    @Override
    public void setXMLDocument(Document document) {
        mDocument = document;
        notifyDataSetChanged();
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder {
        private final TextView item;
        private String id;

        private ArticleViewHolder(final View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.item);
            id = "";

            /* Clique sur un renforcement */
            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(itemView.getContext(), ActivityBuilder.class);
                intent.putExtra("id", id);
                itemView.getContext().startActivity(intent);
            });
        }

        private void setElement(Element element) {
            item.setText(element.getElementsByTagName("title").item(0).getTextContent());
            id = element.getElementsByTagName("id").item(0).getTextContent();
        }
    }
}
