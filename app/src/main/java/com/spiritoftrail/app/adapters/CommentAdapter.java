package com.spiritoftrail.app.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.spiritoftrail.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> implements XMLAsyncTask.DocumentConsumer {

    private Document mDocument = null;

    @Override
    public int getItemCount() {
        if(mDocument != null)
            return mDocument.getElementsByTagName("item").getLength();
        else
            return 0;
    }

    @Override
    @NonNull
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_comment, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        Element item = (Element) mDocument.getElementsByTagName("item").item(position);
        holder.setElement(item);
    }

    @Override
    public void setXMLDocument(Document document) {
        mDocument = document;
        notifyDataSetChanged();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        private final ProgressBar progress;
        private final ImageView avatar;
        private final TextView firstname;
        private final TextView lastname;
        private final TextView pubDate;
        private final TextView text;

        private CommentViewHolder(final View itemView) {
            super(itemView);
            progress = itemView.findViewById(R.id.progress);
            avatar = itemView.findViewById(R.id.avatar);
            firstname = itemView.findViewById(R.id.firstname);
            lastname = itemView.findViewById(R.id.lastname);
            pubDate = itemView.findViewById(R.id.pubDate);
            text = itemView.findViewById(R.id.text);
        }

        private void setElement(Element element) {
            String uid = element.getElementsByTagName("uid").item(0).getTextContent();
            firstname.setText(element.getElementsByTagName("firstname").item(0).getTextContent());
            lastname.setText(element.getElementsByTagName("lastname").item(0).getTextContent());
            pubDate.setText(element.getElementsByTagName("date").item(0).getTextContent());
            text.setText(element.getElementsByTagName("text").item(0).getTextContent());
            Picasso.with(itemView.getContext()).load("https://spiritoftrail.fr/upload/profil/" + uid + ".png").into(avatar, new Callback() {
                @Override
                public void onSuccess() {
                    progress.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progress.setVisibility(View.GONE);
                    avatar.setImageResource(R.drawable.avatar_defaut);
                }
            });
        }
    }
}
