package com.spiritoftrail.app.adapters;

import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.spiritoftrail.app.R;
import com.spiritoftrail.app.activity.ActivityComment;
import com.spiritoftrail.app.utils.BBCode;
import com.spiritoftrail.app.utils.ParseImage;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ArticleViewHolder> implements XMLAsyncTask.DocumentConsumer {

    private Document mDocument = null;

    @Override
    public int getItemCount() {
        if(mDocument != null)
            return mDocument.getElementsByTagName("item").getLength();
        else
            return 0;
    }

    @Override
    @NonNull
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_story, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        Element item = (Element) mDocument.getElementsByTagName("item").item(position);
        holder.setElement(item);
    }

    @Override
    public void setXMLDocument(Document document) {
        mDocument = document;
        notifyDataSetChanged();
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder {
        private final ProgressBar progress;
        private final ImageView avatar;
        private final TextView firstname;
        private final TextView lastname;
        private final TextView pubDate;
        private final TextView title;
        private final TextView description;
        private final Button comment;
        private final ShareButton share;
        private String id;

        private ArticleViewHolder(final View itemView) {
            super(itemView);
            progress = itemView.findViewById(R.id.progress);
            avatar = itemView.findViewById(R.id.avatar);
            firstname = itemView.findViewById(R.id.firstname);
            lastname = itemView.findViewById(R.id.lastname);
            pubDate = itemView.findViewById(R.id.pubDate);
            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            comment = itemView.findViewById(R.id.comment);
            share = itemView.findViewById(R.id.share);
            id = "";

            /* Clique sur les commentaires */
            comment.setOnClickListener(view -> {
                Intent intent = new Intent(itemView.getContext(), ActivityComment.class);
                intent.putExtra("id", id);
                intent.putExtra("page", "story");
                itemView.getContext().startActivity(intent);
            });
        }

        private void setElement(Element element) {
            String uid = element.getElementsByTagName("uid").item(0).getTextContent();
            firstname.setText(element.getElementsByTagName("firstname").item(0).getTextContent());
            lastname.setText(element.getElementsByTagName("lastname").item(0).getTextContent());
            pubDate.setText(element.getElementsByTagName("pubDate").item(0).getTextContent());
            title.setText(element.getElementsByTagName("title").item(0).getTextContent());
            description.setText(Html.fromHtml(BBCode.decode(element.getElementsByTagName("description").item(0).getTextContent()), new ParseImage(itemView.getContext(), description), null));
            description.setMovementMethod(LinkMovementMethod.getInstance());
            id = element.getElementsByTagName("id").item(0).getTextContent();

            /* Bouton partager */
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse("http://spiritoftrail.fr/sorties/show.php?id=" + id))
                    .build();
            share.setShareContent(content);

            /* Image de profil */
            Picasso.with(itemView.getContext()).load("https://spiritoftrail.fr/upload/profil/" + uid + ".png").into(avatar, new Callback() {
                @Override
                public void onSuccess() {
                    progress.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progress.setVisibility(View.GONE);
                    avatar.setImageResource(R.drawable.avatar_defaut);
                }
            });

            int count = Integer.parseInt(element.getElementsByTagName("comment").item(0).getTextContent());
            String s;
            if(count > 1)
                s = count + " commentaires";
            else
                s = count + " commentaire";

            comment.setText(s);
        }
    }
}
