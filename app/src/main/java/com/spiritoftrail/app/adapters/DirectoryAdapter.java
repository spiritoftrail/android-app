package com.spiritoftrail.app.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.spiritoftrail.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.ArticleViewHolder> implements XMLAsyncTask.DocumentConsumer {

    private Document mDocument = null;

    @Override
    public int getItemCount() {
        if(mDocument != null)
            return mDocument.getElementsByTagName("item").getLength();
        else
            return 0;
    }

    @Override
    @NonNull
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_directory, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        Element item = (Element) mDocument.getElementsByTagName("item").item(position);
        holder.setElement(item);
    }

    @Override
    public void setXMLDocument(Document document) {
        mDocument = document;
        notifyDataSetChanged();
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder {
        private final TextView firstname;
        private final TextView lastname;
        private final TextView category;
        private final ImageView avatar;

        private ArticleViewHolder(final View itemView) {
            super(itemView);
            firstname = itemView.findViewById(R.id.firstname);
            lastname = itemView.findViewById(R.id.lastname);
            category = itemView.findViewById(R.id.category);
            avatar = itemView.findViewById(R.id.avatar);
        }

        private void setElement(Element element) {
            String uid = element.getElementsByTagName("uid").item(0).getTextContent();
            firstname.setText(element.getElementsByTagName("firstname").item(0).getTextContent());
            lastname.setText(element.getElementsByTagName("lastname").item(0).getTextContent());
            category.setText(element.getElementsByTagName("category").item(0).getTextContent());

            Picasso.with(itemView.getContext()).load("https://spiritoftrail.fr/upload/profil/" + uid + ".png").into(avatar, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    avatar.setImageResource(R.drawable.avatar_defaut);
                }
            });
        }
    }
}
