package com.spiritoftrail.app.adapters;

import android.widget.TextView;

import com.spiritoftrail.app.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ChallengeRankingAdapter extends RecyclerView.Adapter<ChallengeRankingAdapter.TweetViewHolder> {

    private final List<ChallengeRanking> rankings;

    public ChallengeRankingAdapter(List<ChallengeRanking> rankings) {
        this.rankings = rankings;
    }

    @NonNull
    @Override
    public TweetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_challenge_ranking, parent, false);

        return new TweetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TweetViewHolder viewHolder, final int position) {
        final ChallengeRanking ranking = rankings.get(position);

        if(ranking != null) {
            switch (ranking.getRank()) {
                case 1 -> viewHolder.rank.setBackgroundResource(R.drawable.border_ranking_gold);
                case 2 -> viewHolder.rank.setBackgroundResource(R.drawable.border_ranking_silver);
                case 3 -> viewHolder.rank.setBackgroundResource(R.drawable.border_ranking_bronze);
                default -> viewHolder.rank.setBackgroundResource(R.drawable.border_ranking_unclassified);
            }

            String srank = "" + ranking.getRank();
            String scat = "" + ranking.getCategory();
            String spoints = "" + ranking.getPoints();

            viewHolder.rank.setText(srank);
            viewHolder.lastname.setText(ranking.getLastname());
            viewHolder.firstname.setText(ranking.getFirstname());
            viewHolder.category.setText(scat);
            viewHolder.sexe.setText(ranking.getSexe());
            viewHolder.points.setText(spoints);
        }
    }

    @Override
    public int getItemCount() {
        return rankings.size();
    }

    public class TweetViewHolder extends RecyclerView.ViewHolder {
        private final TextView rank;
        private final TextView lastname;
        private final TextView firstname;
        private final TextView category;
        private final TextView sexe;
        private final TextView points;

        private TweetViewHolder(final View view) {
            super(view);

            rank = itemView.findViewById(R.id.rank);
            lastname = itemView.findViewById(R.id.lastname);
            firstname = itemView.findViewById(R.id.firstname);
            category = itemView.findViewById(R.id.category);
            sexe = itemView.findViewById(R.id.sexe);
            points = itemView.findViewById(R.id.points);
        }
    }
}
