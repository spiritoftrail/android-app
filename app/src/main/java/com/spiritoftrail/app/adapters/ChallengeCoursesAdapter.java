package com.spiritoftrail.app.adapters;

import android.widget.TextView;

import com.spiritoftrail.app.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ChallengeCoursesAdapter extends RecyclerView.Adapter<ChallengeCoursesAdapter.TweetViewHolder> {

    private final List<ChallengeCourses> courses;

    public ChallengeCoursesAdapter(List<ChallengeCourses> courses) {
        this.courses = courses;
    }

    @NonNull
    @Override
    public TweetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_challenge_courses, parent, false);

        return new TweetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TweetViewHolder viewHolder, final int position) {
        final ChallengeCourses course = courses.get(position);

        if(course != null) {
            viewHolder.date.setText(course.getDate());
            viewHolder.name.setText(course.getName());
        }
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public class TweetViewHolder extends RecyclerView.ViewHolder {
        private final TextView date;
        private final TextView name;

        private TweetViewHolder(final View view) {
            super(view);

            date = itemView.findViewById(R.id.date);
            name = itemView.findViewById(R.id.name);
        }
    }
}
