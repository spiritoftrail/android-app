package com.spiritoftrail.app.adapters;

public class ChallengeCourses {
    private final int day;
    private final int month;
    private final String name;

    private final String[] smth = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"};

    public ChallengeCourses(int day, int month, String name) {
        this.day = day;
        this.month = month;
        this.name = name;
    }

    public String getDate() {
        return day + " " + smth[month - 1];
    }

    public String getName() {
        return name;
    }
}
