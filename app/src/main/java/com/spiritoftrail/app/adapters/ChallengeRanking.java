package com.spiritoftrail.app.adapters;

public class ChallengeRanking {
    private final String firstname;
    private final String lastname;
    private final String category;
    private final String sexe;

    private final int rank;
    private final int points;
    private final int nraces;

    public ChallengeRanking(String firstname, String lastname, String category, String sexe, int rank, int points, int nraces) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.category = category;
        this.sexe = sexe;
        this.rank = rank;
        this.points = points;
        this.nraces = nraces;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getCategory() {
        return category;
    }

    public String getSexe() {
        return sexe;
    }

    public int getRank() {
        return rank;
    }

    public int getPoints() {
        return points;
    }

    public int getNraces() {
        return nraces;
    }
}
