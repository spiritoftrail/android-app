package com.spiritoftrail.app.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import androidx.core.app.NotificationCompat;

import com.spiritoftrail.app.R;
import com.spiritoftrail.app.activity.ActivityBuilding;
import com.spiritoftrail.app.activity.ActivityNews;
import com.spiritoftrail.app.activity.ActivitySheets;
import com.spiritoftrail.app.activity.ActivityStories;
import com.spiritoftrail.app.activity.ActivityTrainers;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class FirebaseNotificationService extends FirebaseMessagingService {

    String title;
    String data;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Map<String,String> map = remoteMessage.getData();
        for(String key : map.keySet()) {
            title = key;
            data = map.get(key);
        }

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(data)
                        .setAutoCancel(true);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent intent = switch (title) {
            case "News" -> new Intent(this, ActivityNews.class);
            case "Entrainements" -> new Intent(this, ActivityTrainers.class);
            case "Récits" -> new Intent(this, ActivityStories.class);
            case "Renforcements" -> new Intent(this, ActivityBuilding.class);
            case "Fiches Techniques" -> new Intent(this, ActivitySheets.class);
            default -> new Intent(this, ActivityNews.class);
        };

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(resultPendingIntent);

        manager.notify(0, builder.build());
    }
}