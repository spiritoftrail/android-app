package com.spiritoftrail.app.menu;

import android.content.Context;

import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import androidx.appcompat.app.AppCompatActivity;

import com.spiritoftrail.app.R;
import com.spiritoftrail.app.session.SessionManager;
import com.spiritoftrail.app.utils.CircleTransform;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class SMenu extends AppCompatActivity {

    Context context;
    NavigationView navigationView;

    public SMenu(Context context, NavigationView navigationView) {
        this.context = context;
        this.navigationView = navigationView;
    }

    public void setMenu() {
        /* Do nothing */
    }

    public void setAppBarLayout(SessionManager session) {
        View view = navigationView.getHeaderView(0);

        TextView name = view.findViewById(R.id.name);
        final ImageView avatar = view.findViewById(R.id.avatar);

        String fullname = session.getFirstname() + " " + session.getLastname();
        name.setText(fullname);

        Glide.with(context).load("https://spiritoftrail.fr/upload/profil/" + session.getId() + ".png").listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                Glide.with(context).load(R.drawable.avatar_defaut).transform(new CircleTransform(context)).skipMemoryCache(true).into(avatar);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                return false;
            }
        }).transform(new CircleTransform(context)).into(avatar);
    }
}
